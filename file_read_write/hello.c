#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/init.h>		/* Needed for the macros */

#include <linux/fs.h>		/* open, close, O_WRONLY, O_CREAT */
#include <asm/uaccess.h>	/* set_fs(), get_fs() */
#include <linux/sched.h>	/* current access in get_fs() */
//#include "hello.h"

static void write_file(char *filename, char *buf){
    struct file *f;
    loff_t pos = 0; // offset of buf.
    mm_segment_t old_fs = get_fs();

    set_fs(KERNEL_DS);

    f = filp_open(filename, O_WRONLY|O_CREAT, 0644);

    if (f) {
	vfs_write(f, buf, strlen(buf), &pos);
	filp_close(f, NULL);
    }

    set_fs(old_fs);
}

static void read_file(char *filename)
{
    struct file *f;
    char buf[1];
    loff_t pos = 0; // offset of buf.
    mm_segment_t old_fs = get_fs();

    set_fs(KERNEL_DS);

    f = filp_open(filename, O_RDONLY, 0);

    if (f){
	while (vfs_read(f, buf, 1, &pos) == 1)
	    printk (KERN_CONT "%c", buf[0]);
	printk("");

	filp_close(f, NULL);
    }

    set_fs(old_fs);
}

static int __init hello_init(void)
{
    printk(KERN_INFO "Hello module started.\n");

    // Write
    write_file ("/tmp/hello_test", "It's just yulistic!\n");
    printk(KERN_INFO "Write done.\n");

    // Read
    printk(KERN_INFO "Let's read file\n");
    read_file ("/tmp/hello_test");
    printk(KERN_INFO "Read done.\n");

    return 0;
}

static void __exit hello_exit(void)
{
	printk(KERN_INFO "Goodbye, from hello.\n");
}

module_init(hello_init);
module_exit(hello_exit);
