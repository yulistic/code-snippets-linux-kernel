#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/init.h>		/* Needed for the macros */

//#include "hello.h"

#include <linux/io.h>		/* phys_to_virt */

#define MEMORY_MAP_ADDRS    0x100000000	    // From 4GB to 8GB
#define MEMORY_MAP_SIZE	    0x100000000	    // 4 * 1024 * 1024 * 1024 = 4GB

static int __init hello_init(void)
{
    unsigned long long int *back_store_ptr;	    // currently it is reserved DRAM space.

    printk(KERN_INFO "hello module started.\n");
    
    back_store_ptr = ioremap_nocache(MEMORY_MAP_ADDRS, MEMORY_MAP_SIZE);


    printk(KERN_INFO "Let's map phys to virt.\n");

    //va = phys_to_virt(PFN_PHYS(pa));

    // print address.
    printk(KERN_INFO "back_store_ptr:%p pa:%lx", back_store_ptr, MEMORY_MAP_ADDRS);

    // read value.
    printk(KERN_INFO "*back_store_ptr:%llx", *back_store_ptr);

    // write value.
    *back_store_ptr = 0xdeadbeefdeaddeaf;
    printk(KERN_INFO "*back_store_ptr after write:%llx", *back_store_ptr);

    return 0;
}

static void __exit hello_exit(void)
{
	printk(KERN_INFO "Goodbye, from hello.\n");
}

module_init(hello_init);
module_exit(hello_exit);
